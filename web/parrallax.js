var TitleModel = function() {
        "use strict";
        var a = function() {
            this.defaultTitle = "Soyuz Coffee Roasting", this.current = ""
        };
        return a.prototype.set = function(a) {
            var b = this.defaultTitle;
            a && "" !== a && (b = " | " + b), null !== a && (a = a.capitalize()), document.title = a + b
        }, a
    }(),
    CoffeeWheel = function() {
        "use strict";
        var a = function() {};
        return a.prototype.init = function() {
            function a(b, c) {
                return b === c ? !0 : b.children ? b.children.some(function(b) {
                    return a(b, c)
                }) : !1
            }

            function b(a) {
                if (a.children) {
                    var c = a.children.map(b),
                        d = d3.hsl(c[0]),
                        e = d3.hsl(c[1]);
                    return d3.hsl((d.h + e.h) / 2, 1.2 * d.s, d.l / 1.2)
                }
                return a.colour || "#fff"
            }

            function c(a) {
                var b = d(a),
                    c = d3.interpolate(i.domain(), [a.x, a.x + a.dx]),
                    e = d3.interpolate(j.domain(), [a.y, b]),
                    f = d3.interpolate(j.range(), [a.y ? 20 : 0, h]);
                return function(a) {
                    return function(b) {
                        return i.domain(c(b)), j.domain(e(b)).range(f(b)), p(a)
                    }
                }
            }

            function d(a) {
                return a.children ? Math.max.apply(Math, a.children.map(d)) : a.y + a.dy
            }

            function e(a) {
                return .299 * a.r + .587 * a.g + .114 * a.b
            }
            if (!(window.innerWidth < 860)) {
                var f = 840,
                    g = f,
                    h = f / 2,
                    i = d3.scale.linear().range([0, 2 * Math.PI]),
                    j = d3.scale.pow().exponent(1.3).domain([0, 1]).range([0, h]),
                    k = 5,
                    l = 1e3,
                    m = d3.select("#aromawheel");
                m.select("img").remove();
                var n = m.append("svg").attr("width", f + 2 * k).attr("height", g + 2 * k).append("g").attr("transform", "translate(" + [h + k, h + k] + ")");
                m.append("p").attr("id", "intro").text("");
                var o = d3.layout.partition().sort(null).value(function(a) {
                        return 5.8 - a.depth
                    }),
                    p = d3.svg.arc().startAngle(function(a) {
                        return Math.max(0, Math.min(2 * Math.PI, i(a.x)))
                    }).endAngle(function(a) {
                        return Math.max(0, Math.min(2 * Math.PI, i(a.x + a.dx)))
                    }).innerRadius(function(a) {
                        return Math.max(0, a.y ? j(a.y) : a.y)
                    }).outerRadius(function(a) {
                        return Math.max(0, j(a.y + a.dy))
                    });
                d3.json("/js/wheel.json", function(d, f) {
                    function g(b) {
                        m.transition().duration(l).attrTween("d", c(b)), q.style("visibility", function(c) {
                            return a(b, c) ? null : d3.select(this).style("visibility")
                        }).transition().duration(l).attrTween("text-anchor", function(a) {
                            return function() {
                                return i(a.x + a.dx / 2) > Math.PI ? "end" : "start"
                            }
                        }).attrTween("transform", function(a) {
                            var b = (a.name || "").split(" ").length > 1;
                            return function() {
                                var c = 180 * i(a.x + a.dx / 2) / Math.PI - 90,
                                    d = c + (b ? -.5 : 0);
                                return "rotate(" + d + ")translate(" + (j(a.y) + k) + ")rotate(" + (c > 90 ? -180 : 0) + ")"
                            }
                        }).style("fill-opacity", function(c) {
                            return a(b, c) ? 1 : 1e-6
                        }).each("end", function(c) {
                            d3.select(this).style("visibility", a(b, c) ? null : "hidden")
                        })
                    }
                    var h = o.nodes({
                            children: f
                        }),
                        m = n.selectAll("path").data(h);
                    m.enter().append("path").attr("id", function(a, b) {
                        return "path-" + b
                    }).attr("d", p).attr("fill-rule", "evenodd").style("fill", b).on("click", g);
                    var q = n.selectAll("text").data(h),
                        r = q.enter().append("text").style("fill-opacity", 1).style("fill", function(a) {
                            return e(d3.rgb(b(a))) < 125 ? "#eee" : "#000"
                        }).attr("text-anchor", function(a) {
                            return i(a.x + a.dx / 2) > Math.PI ? "end" : "start"
                        }).attr("dy", ".2em").attr("transform", function(a) {
                            var b = (a.name || "").split(" ").length > 1,
                                c = 180 * i(a.x + a.dx / 2) / Math.PI - 90,
                                d = c + (b ? -.5 : 0);
                            return "rotate(" + d + ")translate(" + (j(a.y) + k) + ")rotate(" + (c > 90 ? -180 : 0) + ")"
                        }).on("click", g);
                    r.append("tspan").attr("x", 0).text(function(a) {
                        return a.depth ? a.name.split(" ")[0] : ""
                    }), r.append("tspan").attr("x", 0).attr("dy", "1em").text(function(a) {
                        return a.depth ? a.name.split(" ")[1] || "" : ""
                    })
                })
            }
        }, a.prototype.unload = function() {}, a
    }(),
    Icons = function() {
        var a = function() {};
        return a.prototype.init = function() {
            function a(a, b) {
                e = d, d = a.attr("targetContent"), $("#" + d).css("visibility", "visible"), $("#" + d).css("display", "block"), $("#" + d).css("margin-top", "0px");
                var f = $(b.currentTarget).find(".icon-image"),
                    g = colorModel.getRandomColor(),
                    h = b.pageX - f[0].offsetLeft - f.offset().left,
                    i = b.pageY - f[0].offsetTop - f.offset().top,
                    j = {},
                    k = {},
                    l = f.next().find(".icon-effect");
                colorModel.setPageColor(g.name), TweenMax.set(l, {
                    backgroundColor: g.light
                }), 100 > h && 100 > i ? (j.top = "0%", k.top = "0%", j.left = "-100%", k.left = "0%") : h > 100 && 100 > i ? (j.top = "-100%", k.top = "0%", j.left = "0%", k.left = "0%") : h > 100 && i > 100 ? (j.top = "0%", k.top = "0%", j.left = "100%", k.left = "0%") : 100 > h && i > 100 && (j.top = "100%", k.top = "0%", j.left = "0%", k.left = "0%"), TweenMax.fromTo(l, .2, j, k), c(d)
            }

            function b(a, b) {
                var c = $(".icon-image"),
                    d = b.pageX - c[0].offsetLeft - c.offset().left,
                    e = b.pageY - c[0].offsetTop - c.offset().top,
                    f = {},
                    g = c.next().find(".icon-effect");
                100 > d && 100 > e ? f.left = "-100%" : d > 100 && 100 > e ? f.top = "-100%" : d > 100 && e > 100 ? f.left = "100%" : 100 > d && e > 100 && (f.top = "100%"), TweenMax.to(g, .2, f)
            }

            function c(a) {
                a != e && (TweenMax.set($("#" + a), {
                    autoAlpha: 1,
                    marginLeft: 0,
                    display: "block"
                }), TweenMax.set($("#" + e), {
                    autoAlpha: 0,
                    marginLeft: 0,
                    display: "none"
                }))
            }
            var d, e, f = [],
                g = [];
            TweenMax.delayedCall(1, function() {
                $(".icon").each(function(c) {
                    $(this).attr("id", "icon" + c), $(this).attr("targetContent", "icon-content" + c), Modernizr.touch ? $(this).click(function(b) {
                        a($(this), b, !1)
                    }) : ($(this).mouseenter(function(b) {
                        a($(this), b, !0)
                    }), $(this).mouseleave(function(a) {
                        b($(this), a, !0)
                    })), f.push($(this))
                }), $(".icon-content").each(function(a) {
                    $(this).attr("id", "icon-content" + a), $(this).css("margin", "0 auto"), $(this).css("margin-bottom", "100px"), g.push($(this))
                }), d = "icon-content0", c(d)
            })
        }, a
    }(window),
    SpiderGraph = function() {
        var a = function(a, b, c) {
            var d = a.getContext("2d"),
                c = c.split(",") || [];
            b = b || "#00cc00";
            var e = {
                    labels: ["Aroma", "Acidity", "Sweetness", "Body", "Flavor", "Aftertaste"],
                    datasets: [{
                        fillColor: "rgba(255,255,255,1)",
                        strokeColor: "rgba(255,255,255,1)",
                        pointColor: "rgba(255,255,255,1)",
                        pointStrokeColor: "#000",
                        data: c
                    }]
                },
                f = {
                    scaleOverlay: !0,
                    scaleOverride: !0,
                    scaleSteps: 5,
                    scaleStepWidth: 2,
                    scaleStartValue: 0,
                    scaleShowLine: !0,
                    scaleLineColor: "rgba(0,0,0,1)",
                    scaleLineWidth: 2,
                    scaleShowLabels: !0,
                    scaleLabel: "<%=value%>",
                    scaleFontFamily: "'Arial'",
                    scaleFontSize: 14,
                    scaleFontStyle: "bold",
                    scaleFontColor: "#000",
                    scaleShowLabelBackdrop: !1,
                    scaleBackdropColor: "rgba(255,255,255,1)",
                    scaleBackdropPaddingY: 2,
                    scaleBackdropPaddingX: 20,
                    angleShowLineOut: !0,
                    angleLineColor: "rgba(0,0,0,1)",
                    angleLineWidth: 2,
                    pointLabelFontFamily: "'Arial'",
                    pointLabelFontStyle: "bold",
                    pointLabelFontSize: 14,
                    pointLabelFontColor: "#000",
                    pointDot: !1,
                    pointDotRadius: 3,
                    pointDotStrokeWidth: 1,
                    datasetStroke: !0,
                    datasetStrokeWidth: 0,
                    datasetFill: !0,
                    animation: !0,
                    animationSteps: 20,
                    animationEasing: "easeOutBack",
                    onAnimationComplete: null,
                    showBackground: !0,
                    backgroundColor: b,
                    outerLineWidth: 8
                };
            return new Chart(d).Radar(e, f)
        };
        return a
    }(),
    Slider = function() {
        "use strict";
        var a = function() {
            var a = this;
            $("#wrapper, #blog-content").on({
                mouseover: function() {
                    a.navigate_to($(this))
                }
            }, ".slider-nav")
        };
        return a.prototype.navigate_to = function(a) {
            var b = a.index(),
                c = a.parents(".slider");
            c.find("li").removeClass("active"), c.find("li:eq(" + b + ")").addClass("active"), a.addClass("active")
        }, a
    }(),
    ColorModel = function() {
        "use strict";
        var a = function() {
            this.counter = 0, this.currentColor = "", this.colors = [{
                name: "gray",
                normal: "#7C828F",
                light: "#A7AAB3"
            }, {
                name: "blue",
                normal: "#768E99",
                light: "#A4B2BA"
            }, {
                name: "green",
                normal: "#9D9F93",
                light: "#BFBEB6"
            }, {
                name: "red",
                normal: "#B6A69C",
                light: "#C8BCB4"
            }, {
                name: "orange",
                normal: "#C0AA81",
                light: "#D5C6AB"
            }, {
                name: "yellow",
                normal: "#D9BC86",
                light: "#E5D3B0"
            }]
        };
        return a.prototype.getRandomColor = function() {
            return this.counter++, this.counter >= this.colors.length && (this.counter = 0), this.colors[this.counter]
        }, a.prototype.getColors = function() {
            return this.colors
        }, a.prototype.getColorByName = function(a) {
            var b = $.grep(this.colors, function(b) {
                return b.name == a
            });
            return b[0]
        }, a.prototype.setPageColor = function(a) {
            $(".extend-page").removeClass(this.currentColor), $(".extend-page").addClass(a);
            var b = this.getColorByName(a);
            TweenMax.to($(".extend-page"), .8, {
                backgroundColor: b.normal
            }), this.currentColor = a
        }, a
    }(),
    City = function() {
        "use strict";
        var a = function(a, b, c) {
            var d = "name_" + window.language;
            this.name = b[d] || "", this.x = 0 | b.x || 0, this.y = 0 | b.y || 0, this.shops = c || [], this.region_id = b.region_id || 0, this.id = a, this.el = document.createElement("div"), this.el.setAttribute("class", "city"), this.el.setAttribute("data-id", a), this.el.style.left = this.x + "px", this.el.style.top = this.y + "px", this.el.style.visibility = "hidden", this.dot = document.createElement("div"), this.dot.setAttribute("class", "dot");
            for (var e = '<div class="shops"><ul>', f = 0, g = this.shops, h = g.length; h > f; f++) e += '<li><a href="' + g[f].site + '" target="_blank">' + g[f][d] + "</a></li>";
            return e += "</ul></div>", this.list = $(e), this.el.appendChild(this.dot), this.el.appendChild($('<div class="name"><span>' + this.name + "</span></div<")[0]), this.el.appendChild(this.list[0]), this.nameEl = $(this.el).find(".name"), this
        };
        return a.prototype.animateIn = function(a) {
            TweenMax.fromTo(this.el, .3, {
                scale: 5,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                delay: .3 + .01 * a
            })
        }, a.prototype.animateOut = function() {
            TweenMax.to(this.el, .3, {
                scale: 0,
                overwrite: 1
            })
        }, a.prototype.animateOver = function() {
            window.innerWidth < 860 || (TweenMax.set(this.el, {
                zIndex: 20
            }), TweenMax.to(this.nameEl, .2, {
                width: this.nameEl.find("span").outerWidth()
            }), TweenMax.to(this.list, .2, {
                width: 120
            }))
        }, a.prototype.animateLeave = function() {
            window.innerWidth < 860 || (TweenMax.set(this.el, {
                zIndex: 10
            }), TweenMax.to(this.nameEl, .2, {
                width: "0px"
            }), TweenMax.to(this.list, .2, {
                width: "0px"
            }))
        }, a
    }(),
    Map = function() {
        "use strict";
        var a = function(a, b) {
            var c = this;
            this.el = document.createElement("div"), this.el.setAttribute("class", "map"), this.el.style.display = "none", this.cities = [];
            for (var d = 0, e = a.cities || [], f = e.length; f > d; d++) {
                for (var g = this.getCity(e[d].city), h = [], i = 0, j = e[d].shops, k = j.length; k > i; i++) h.push(this.getShop(j[i]));
                var l = new City(d, g, h);
                this.cities.push(l), this.el.appendChild(l.el)
            }
            this.cities.sort(function(a, b) {
                return a.name > b.name ? 1 : a.name < b.name ? -1 : 0
            });
            var m = document.createElement("select");
            for (m.appendChild(document.createElement("option")), d = 0; d < this.cities.length; d++) {
                l = this.cities[d];
                var n = document.createElement("option");
                n.value = l.id, n.appendChild(document.createTextNode(l.name)), m.appendChild(n)
            }
            $(this.el).prepend(m), b.appendChild(this.el), $(m).on({
                change: function() {
                    if ("" == $(this).val()) return void TweenMax.set($(c.el).find(".city"), {
                        display: "block"
                    });
                    var a = $(this).val();
                    TweenMax.set($(c.el).find(".city"), {
                        display: "none"
                    }), TweenMax.set($(c.el).find(".city[data-id=" + a + "]"), {
                        display: "block"
                    })
                }
            }), $(this.el).on({
                mouseover: function(a) {
                    var b = a.currentTarget.getAttribute("data-id") || 0;
                    c.cities[b].animateOver()
                },
                mouseleave: function(a) {
                    var b = a.currentTarget.getAttribute("data-id") || 0;
                    c.cities[b].animateLeave()
                }
            }, ".city")
        };
        return a.prototype.getShop = function(a) {
            var b = [{
                    id: 1,
                    name_en: "Lenta",
                    name_ru: "Лента",
                    site: "http://www.lenta.com"
                }, {
                    id: 2,
                    name_en: "Azbuka Vkusa",
                    name_ru: "Азбука Вкуса",
                    site: "http://www.azbukavkusa.ru"
                }, {
                    id: 3,
                    name_en: "Victoria",
                    name_ru: "Виктория",
                    site: "http://www.victoria-group.ru"
                }, {
                    id: 4,
                    name_en: "Globus",
                    name_ru: "Глобус",
                    site: "http://www.globus.ru"
                }, {
                    id: 5,
                    name_en: "SELGROS",
                    name_ru: "Зельгрос",
                    site: "http://www.selgros.ru"
                }, {
                    id: 6,
                    name_en: "7-ya",
                    name_ru: "Интерторг",
                    site: "http://www.7-ya.ru"
                }, {
                    id: 7,
                    name_en: "Matrix",
                    name_ru: "Матрица",
                    site: "http://www.matrix-m.ru"
                }, {
                    id: 8,
                    name_en: "Mango Super",
                    name_ru: "Манго Супер",
                    site: "http://www.mangosm.ru"
                }, {
                    id: 9,
                    name_en: "Minuta Market",
                    name_ru: "Минута маркет",
                    site: "msk.minutamarket.ru"
                }, {
                    id: 10,
                    name_en: "Stanem Druzyami",
                    name_ru: "Станем друзьями",
                    site: "http://www.semenovsky.ru/catalog/shop/44"
                }, {
                    id: 11,
                    name_en: "Tvoy Dom",
                    name_ru: "Твой Дом",
                    site: "http://www.tvoydom-news.ru/scenter11/department/438"
                }, {
                    id: 12,
                    name_en: "TSVETNOY",
                    name_ru: "Фермер Базар",
                    site: "http://www.tsvetnoy.com/ru/"
                }, {
                    id: 13,
                    name_en: "Holding Center",
                    name_ru: "ХЦ",
                    site: "http://www.rus.hcdom.ru/other/supermarkets/"
                }, {
                    id: 14,
                    name_en: "Utkonos",
                    name_ru: "Утконос",
                    site: "http://www.utkonos.ru"
                }, {
                    id: 15,
                    name_en: "Universam 24h",
                    name_ru: "Близнецы",
                    site: ""
                }, {
                    id: 16,
                    name_en: "TD Real",
                    name_ru: "РеалЪ Свежие Продукты",
                    site: "http://www.tdreal.spb.ru"
                }, {
                    id: 17,
                    name_en: "Spar",
                    name_ru: "Спар",
                    site: "http://www.spar.ru"
                }, {
                    id: 18,
                    name_en: "7ya ",
                    name_ru: "7-Я ",
                    site: "http://www.7-ya.ru"
                }, {
                    id: 19,
                    name_en: "IDEA",
                    name_ru: "ИдеЯ",
                    site: "http://www.7-ya.ru"
                }, {
                    id: 20,
                    name_en: "Lukoshko",
                    name_ru: "Лукошко",
                    site: ""
                }, {
                    id: 21,
                    name_en: "Gatronom 811",
                    name_ru: "Гастроном 811",
                    site: "http://www.gastronom811.ru"
                }, {
                    id: 22,
                    name_en: "Season",
                    name_ru: "Сезон",
                    site: ""
                }, {
                    id: 23,
                    name_en: "Frunzenskiy",
                    name_ru: "Фрунзенский",
                    site: ""
                }, {
                    id: 24,
                    name_en: "Nevskiy",
                    name_ru: "Невский",
                    site: ""
                }, {
                    id: 25,
                    name_en: "Diet 18",
                    name_ru: "Диета 18",
                    site: "shops.dieta18.ru"
                }, {
                    id: 26,
                    name_en: "Lime Fresh Market",
                    name_ru: "Лайм Фреш Маркет",
                    site: ""
                }, {
                    id: 27,
                    name_en: "Stockmann",
                    name_ru: "Стокманн",
                    site: "http://www.stockmann.ru"
                }, {
                    id: 28,
                    name_en: "Plovdiv",
                    name_ru: "Пловдив",
                    site: "http://www.plovdiv.su"
                }, {
                    id: 29,
                    name_en: "Land Supermarket",
                    name_ru: "Лэнд",
                    site: "http://www.supermarket-land.ru"
                }, {
                    id: 30,
                    name_en: "Vkusomir",
                    name_ru: "Вкусомир",
                    site: "http://www.vkusomir.ru"
                }, {
                    id: 31,
                    name_en: "Lotos",
                    name_ru: "Лотос",
                    site: "http://www.mlotos.ru"
                }, {
                    id: 32,
                    name_en: "Evroros",
                    name_ru: "Евророс",
                    site: "http://www.evroros.ru"
                }, {
                    id: 33,
                    name_en: "Holiday Classic",
                    name_ru: "Холидей",
                    site: "http://www.hclass.ru"
                }, {
                    id: 34,
                    name_en: "Chaykoff",
                    name_ru: "Чайкофф",
                    site: ""
                }, {
                    id: 35,
                    name_en: "Maria-Ra",
                    name_ru: "Мария Ра",
                    site: "http://www.maria-ra.ru"
                }, {
                    id: 36,
                    name_en: "AKKOND",
                    name_ru: "Акконд",
                    site: "http://www.akkond.ru"
                }, {
                    id: 37,
                    name_en: "Bystronom",
                    name_ru: "Быстроном",
                    site: "http://www.bystronom.ru"
                }, {
                    id: 38,
                    name_en: "Bahetle",
                    name_ru: "Бахетле",
                    site: "http://www.bahetle.com"
                }, {
                    id: 39,
                    name_en: "Supermarket",
                    name_ru: "Супермаркет",
                    site: ""
                }, {
                    id: 40,
                    name_en: "Victoria",
                    name_ru: "Виктория",
                    site: "http://www.victoria-group.ru"
                }, {
                    id: 41,
                    name_en: "IZHTRADING",
                    name_ru: "ИжТрейдинг",
                    site: "http://www.izhtrading.ru"
                }, {
                    id: 42,
                    name_en: "Gastronom 18",
                    name_ru: "Гастроном 18",
                    site: "http://www.gastronom18.ru"
                }, {
                    id: 43,
                    name_en: "Rendezvous",
                    name_ru: "Рандеву",
                    site: ""
                }, {
                    id: 44,
                    name_en: "Vkusnyiy Dom",
                    name_ru: "Вкусный Дом",
                    site: ""
                }, {
                    id: 45,
                    name_en: "Mango Mini",
                    name_ru: "Манго Мини",
                    site: "http://www.mangosm.ru"
                }, {
                    id: 46,
                    name_en: "Zodiac",
                    name_ru: "Зодиак",
                    site: ""
                }, {
                    id: 47,
                    name_en: "Semya",
                    name_ru: "Семья",
                    site: "http://tds-group.ru/"
                }, {
                    id: 48,
                    name_en: "Karusel",
                    name_ru: "Карусель",
                    site: "http://www.karusel.ru/"
                }, {
                    id: 49,
                    name_en: "Magnit",
                    name_ru: "Магнит",
                    site: "http://www.magnit-info.ru/"
                }, {
                    id: 50,
                    name_en: "SMAK",
                    name_ru: "Смак",
                    site: "http://www.good-smak.ru/"
                }],
                c = $.grep(b, function(b) {
                    return b.id == a
                });
            return c[0] || {}
        }, a.prototype.getCity = function(a) {
            var b = [{
                    id: 1,
                    region_id: 1,
                    name_en: "Saint Petersburg",
                    name_ru: "Санкт-Петербург",
                    x: 129,
                    y: 170
                }, {
                    id: 2,
                    region_id: 2,
                    name_en: "Armavir",
                    name_ru: "Армавир",
                    x: 28,
                    y: 395
                }, {
                    id: 3,
                    region_id: 3,
                    name_en: "Astrakhan",
                    name_ru: "Астрахань",
                    x: 96,
                    y: 428
                }, {
                    id: 4,
                    region_id: 4,
                    name_en: "Barnaul",
                    name_ru: "Барнаул",
                    x: 415,
                    y: 474
                }, {
                    id: 5,
                    region_id: 5,
                    name_en: "Belgorod",
                    name_ru: "Белгород",
                    x: 57,
                    y: 315
                }, {
                    id: 6,
                    region_id: 4,
                    name_en: "Biysk",
                    name_ru: "Бийск",
                    x: 427,
                    y: 484
                }, {
                    id: 7,
                    region_id: 6,
                    name_en: "Veliky Novgorod",
                    name_ru: "Великий Новгород",
                    x: 114,
                    y: 185
                }, {
                    id: 8,
                    region_id: 7,
                    name_en: "Volgograd",
                    name_ru: "Волгоград",
                    x: 97,
                    y: 383
                }, {
                    id: 9,
                    region_id: 7,
                    name_en: "Volzhsky",
                    name_ru: "Волжский",
                    x: 113,
                    y: 378
                }, {
                    id: 10,
                    region_id: 14,
                    name_en: "Vologda",
                    name_ru: "Вологда",
                    x: 179,
                    y: 227
                }, {
                    id: 11,
                    region_id: 22,
                    name_en: "Voronezh",
                    name_ru: "Воронеж",
                    x: 88,
                    y: 331
                }, {
                    id: 12,
                    region_id: 2,
                    name_en: "Krasnodar",
                    name_ru: "Краснодар",
                    x: 9,
                    y: 387
                }, {
                    id: 13,
                    region_id: 16,
                    name_en: "Moscow",
                    name_ru: "Москва",
                    x: 121,
                    y: 249
                }, {
                    id: 14,
                    region_id: 25,
                    name_en: "Naberezhnye Chelny",
                    name_ru: "Набережные Челны",
                    x: 207,
                    y: 346
                }, {
                    id: 15,
                    region_id: 17,
                    name_en: "Nizhny Novgorod",
                    name_ru: "Нижний Новгород",
                    x: 154,
                    y: 290
                }, {
                    id: 16,
                    region_id: 2,
                    name_en: "Novorossiysk",
                    name_ru: "Новороссийск",
                    x: 10,
                    y: 402
                }, {
                    id: 17,
                    region_id: 30,
                    name_en: "Novosibirsk",
                    name_ru: "Новосибирск",
                    x: 415,
                    y: 439
                }, {
                    id: 18,
                    region_id: 29,
                    name_en: "Omsk",
                    name_ru: "Омск",
                    x: 350,
                    y: 420
                }, {
                    id: 19,
                    region_id: 24,
                    name_en: "Orenburg",
                    name_ru: "Оренбург",
                    x: 196,
                    y: 409
                }, {
                    id: 20,
                    region_id: 19,
                    name_en: "Penza",
                    name_ru: "Пенза",
                    x: 130,
                    y: 322
                }, {
                    id: 21,
                    region_id: 10,
                    name_en: "Petrozavodsk",
                    name_ru: "Петрозаводск",
                    x: 177,
                    y: 172
                }, {
                    id: 22,
                    region_id: 31,
                    name_en: "Prokopyevsk",
                    name_ru: "Прокопьевск",
                    x: 448,
                    y: 455
                }, {
                    id: 23,
                    region_id: 9,
                    name_en: "Pskov",
                    name_ru: "Псков",
                    x: 84,
                    y: 177
                }, {
                    id: 24,
                    region_id: 34,
                    name_en: "Rostov-on-Don",
                    name_ru: "Ростов-на-Дону",
                    x: 55,
                    y: 372
                }, {
                    id: 25,
                    region_id: 17,
                    name_en: "Ryazan",
                    name_ru: "Рязань",
                    x: 126,
                    y: 284
                }, {
                    id: 26,
                    region_id: 23,
                    name_en: "Saratov",
                    name_ru: "Саратов",
                    x: 131,
                    y: 361
                }, {
                    id: 27,
                    region_id: 28,
                    name_en: "Surgut",
                    name_ru: "Сургут",
                    x: 380,
                    y: 342
                }, {
                    id: 28,
                    region_id: 12,
                    name_en: "Syktyvkar",
                    name_ru: "Сыктывкар",
                    x: 268,
                    y: 252
                }, {
                    id: 29,
                    region_id: 34,
                    name_en: "Taganrog",
                    name_ru: "Таганрог",
                    x: 44,
                    y: 364
                }, {
                    id: 30,
                    region_id: 13,
                    name_en: "Tver",
                    name_ru: "Тверь",
                    x: 118,
                    y: 217
                }, {
                    id: 31,
                    region_id: 21,
                    name_en: "Tolyatti",
                    name_ru: "Тольятти",
                    x: 172,
                    y: 355
                }, {
                    id: 32,
                    region_id: 27,
                    name_en: "Tyumen",
                    name_ru: "Тюмень",
                    x: 312,
                    y: 376
                }, {
                    id: 33,
                    region_id: 20,
                    name_en: "Ulyanovsk",
                    name_ru: "Ульяновск",
                    x: 169,
                    y: 336
                }, {
                    id: 34,
                    region_id: 26,
                    name_en: "Ufa",
                    name_ru: "Уфа",
                    x: 229,
                    y: 371
                }, {
                    id: 35,
                    region_id: 18,
                    name_en: "Cheboksary",
                    name_ru: "Чебоксары",
                    x: 183,
                    y: 310
                }, {
                    id: 36,
                    region_id: 14,
                    name_en: "Cherepovets",
                    name_ru: "Череповец",
                    x: 168,
                    y: 212
                }, {
                    id: 37,
                    region_id: 15,
                    name_en: "Yaroslavl",
                    name_ru: "Ярославль",
                    x: 154,
                    y: 236
                }, {
                    id: 38,
                    region_id: 33,
                    name_en: "Vladimir",
                    name_ru: "Владимир",
                    x: 146,
                    y: 259
                }, {
                    id: 39,
                    region_id: 11,
                    name_en: "Severodvinsk",
                    name_ru: "Северодвинск",
                    x: 236,
                    y: 189
                }, {
                    id: 40,
                    region_id: 32,
                    name_en: "Murmansk",
                    name_ru: "Мурманск",
                    x: 243,
                    y: 100
                }, {
                    id: 41,
                    region_id: 31,
                    name_en: "Kemerovo",
                    name_ru: "Кемерово",
                    x: 441,
                    y: 436
                }, {
                    id: 42,
                    region_id: 8,
                    name_en: "Kaliningrad",
                    name_ru: "Калининград",
                    x: 48,
                    y: 155
                }, {
                    id: 43,
                    region_id: 35,
                    name_en: "Izhevsk",
                    name_ru: "Ижевск",
                    x: 223,
                    y: 327
                }, {
                    id: 44,
                    region_id: 21,
                    name_en: "Samara",
                    name_ru: "Самара",
                    x: 180,
                    y: 363
                }, {
                    id: 45,
                    region_id: 25,
                    name_en: "Kazan",
                    name_ru: "Казань",
                    x: 188,
                    y: 334
                }, {
                    id: 46,
                    region_id: 2,
                    name_en: "Sochi",
                    name_ru: "Сочи",
                    x: 12,
                    y: 412
                }, {
                    id: 47,
                    region_id: 41,
                    name_en: "Lipetsk",
                    name_ru: "Липецк",
                    x: 97,
                    y: 300
                }, {
                    id: 48,
                    region_id: 43,
                    name_en: "Kirov",
                    name_ru: "Киров",
                    x: 220,
                    y: 299
                }, {
                    id: 49,
                    region_id: 42,
                    name_en: "Kostroma",
                    name_ru: "Кострома",
                    x: 172,
                    y: 253
                }, {
                    id: 50,
                    region_id: 39,
                    name_en: "Smolensk",
                    name_ru: "Смоленск",
                    x: 85,
                    y: 218
                }, {
                    id: 51,
                    region_id: 40,
                    name_en: "Novomoskovsk",
                    name_ru: "Новомосковск",
                    x: 105,
                    y: 266
                }, {
                    id: 52,
                    region_id: 17,
                    name_en: "Dzerzhinsk",
                    name_ru: "Дзержинск",
                    x: 148,
                    y: 293
                }, {
                    id: 53,
                    region_id: 46,
                    name_en: "Tambov",
                    name_ru: "Тамбов",
                    x: 111,
                    y: 315
                }, {
                    id: 54,
                    region_id: 44,
                    name_en: "Yoshkar-Ola",
                    name_ru: "Йошкар-Ола",
                    x: 195,
                    y: 308
                }, {
                    id: 55,
                    region_id: 5,
                    name_en: "Stary Oskol",
                    name_ru: "Старый Оскол",
                    x: 70,
                    y: 324
                }, {
                    id: 56,
                    region_id: 21,
                    name_en: "Syzran",
                    name_ru: "Сызрань",
                    x: 169,
                    y: 361
                }, {
                    id: 57,
                    region_id: 36,
                    name_en: "Yekaterinburg",
                    name_ru: "Екатеринбург",
                    x: 279,
                    y: 352
                }, {
                    id: 58,
                    region_id: 37,
                    name_en: "Miass",
                    name_ru: "Миасс",
                    x: 259,
                    y: 390
                }, {
                    id: 59,
                    region_id: 37,
                    name_en: "Chelyabinsk",
                    name_ru: "Челябинск",
                    x: 263,
                    y: 387
                }, {
                    id: 60,
                    region_id: 40,
                    name_en: "Tula",
                    name_ru: "Тула",
                    x: 98,
                    y: 248
                }, {
                    id: 61,
                    region_id: 45,
                    name_en: "Vladikavkaz",
                    name_ru: "Владикавказ",
                    x: 20,
                    y: 461
                }, {
                    id: 62,
                    region_id: 26,
                    name_en: "Salavat",
                    name_ru: "Салават",
                    x: 225,
                    y: 386
                }, {
                    id: 63,
                    region_id: 23,
                    name_en: "Engels",
                    name_ru: "Волжский",
                    x: 131,
                    y: 361
                }, {
                    id: 64,
                    region_id: 38,
                    name_en: "Kurgan",
                    name_ru: "Курган",
                    x: 290,
                    y: 394
                }],
                c = $.grep(b, function(b) {
                    return b.id == a
                });
            return c[0] || {}
        }, a.prototype.animateIn = function() {
            this.el.style.display = "block", this.el.style.height = "auto";
            for (var a = 0, b = this.cities, c = this.cities.length; c > a; a++) b[a].animateIn(a), $(".region-" + b[a].region_id).css({
                visibility: "visible"
            })
        }, a.prototype.animateOut = function() {
            var a = this;
            $("#map-regions").find("img").css({
                visibility: "hidden"
            });
            for (var b = 0, c = this.cities, d = this.cities.length; d > b; b++) c[b].animateOut(b);
            setTimeout(function() {
                $(a.select).val("").trigger("change"), a.el.style.height = 0, a.el.style.display = "none"
            }, 300)
        }, a
    }(),
    MapModel = function() {
        "use strict";
        var a = function() {
            this.initialized = !1, this.activeMap = !1, this.maps = {}, this.products = {
                paretto: {
                    celesto: {
                        cities: [{
                            city: 1,
                            shops: [1, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 48]
                        }, {
                            city: 2,
                            shops: [1]
                        }, {
                            city: 3,
                            shops: [1]
                        }, {
                            city: 4,
                            shops: [34, 35]
                        }, {
                            city: 5,
                            shops: [1]
                        }, {
                            city: 6,
                            shops: [1]
                        }, {
                            city: 7,
                            shops: [1, 49]
                        }, {
                            city: 8,
                            shops: [1, 48, 49]
                        }, {
                            city: 9,
                            shops: [1, 48, 49]
                        }, {
                            city: 10,
                            shops: [1]
                        }, {
                            city: 11,
                            shops: [1, 48]
                        }, {
                            city: 12,
                            shops: [1, 49]
                        }, {
                            city: 13,
                            shops: [1, 3, 5, 7, 8, 9, 10, 11, 18, 48]
                        }, {
                            city: 14,
                            shops: [1, 48]
                        }, {
                            city: 15,
                            shops: [1, 48]
                        }, {
                            city: 16,
                            shops: [1, 49]
                        }, {
                            city: 17,
                            shops: [1]
                        }, {
                            city: 18,
                            shops: [1]
                        }, {
                            city: 19,
                            shops: [1, 48]
                        }, {
                            city: 20,
                            shops: [1, 49]
                        }, {
                            city: 21,
                            shops: [1, 31, 46]
                        }, {
                            city: 22,
                            shops: [1]
                        }, {
                            city: 23,
                            shops: [1]
                        }, {
                            city: 24,
                            shops: [1, 49]
                        }, {
                            city: 25,
                            shops: [1, 48]
                        }, {
                            city: 26,
                            shops: [1, 48]
                        }, {
                            city: 27,
                            shops: [1]
                        }, {
                            city: 28,
                            shops: [1]
                        }, {
                            city: 29,
                            shops: [1]
                        }, {
                            city: 30,
                            shops: [1, 48, 49]
                        }, {
                            city: 31,
                            shops: [1, 49]
                        }, {
                            city: 32,
                            shops: [1, 48, 49]
                        }, {
                            city: 33,
                            shops: [1, 49]
                        }, {
                            city: 34,
                            shops: [1, 48]
                        }, {
                            city: 35,
                            shops: [1, 36, 48, 49, 50]
                        }, {
                            city: 36,
                            shops: [1]
                        }, {
                            city: 37,
                            shops: [1, 48, 49]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 40,
                            shops: [32]
                        }, {
                            city: 41,
                            shops: [33]
                        }, {
                            city: 42,
                            shops: [40, 47]
                        }, {
                            city: 43,
                            shops: [42, 48, 49]
                        }, {
                            city: 44,
                            shops: [48]
                        }, {
                            city: 45,
                            shops: [48]
                        }, {
                            city: 46,
                            shops: [48, 49]
                        }, {
                            city: 47,
                            shops: [48]
                        }, {
                            city: 48,
                            shops: [48]
                        }, {
                            city: 49,
                            shops: [48, 49]
                        }, {
                            city: 50,
                            shops: [48]
                        }, {
                            city: 51,
                            shops: [48]
                        }, {
                            city: 52,
                            shops: [48]
                        }, {
                            city: 53,
                            shops: [48]
                        }, {
                            city: 54,
                            shops: [48]
                        }, {
                            city: 55,
                            shops: [48]
                        }, {
                            city: 56,
                            shops: [48]
                        }, {
                            city: 57,
                            shops: [48]
                        }, {
                            city: 58,
                            shops: [48]
                        }, {
                            city: 59,
                            shops: [48]
                        }, {
                            city: 60,
                            shops: [49]
                        }, {
                            city: 61,
                            shops: [49]
                        }, {
                            city: 62,
                            shops: [49]
                        }, {
                            city: 63,
                            shops: [49]
                        }, {
                            city: 64,
                            shops: [49]
                        }]
                    },
                    divino: {
                        cities: [{
                            city: 1,
                            shops: [1, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 48]
                        }, {
                            city: 2,
                            shops: [1]
                        }, {
                            city: 3,
                            shops: [1]
                        }, {
                            city: 4,
                            shops: [1, 35]
                        }, {
                            city: 5,
                            shops: [1, 48]
                        }, {
                            city: 6,
                            shops: [1]
                        }, {
                            city: 7,
                            shops: [1, 49]
                        }, {
                            city: 8,
                            shops: [1, 48, 49]
                        }, {
                            city: 9,
                            shops: [1, 48, 49]
                        }, {
                            city: 10,
                            shops: [1]
                        }, {
                            city: 11,
                            shops: [1]
                        }, {
                            city: 12,
                            shops: [1, 49]
                        }, {
                            city: 13,
                            shops: [1, 3, 5, 7, 8, 10, 11, 48]
                        }, {
                            city: 14,
                            shops: [1, 48]
                        }, {
                            city: 15,
                            shops: [1, 48]
                        }, {
                            city: 16,
                            shops: [1, 49]
                        }, {
                            city: 17,
                            shops: [1]
                        }, {
                            city: 18,
                            shops: [1]
                        }, {
                            city: 19,
                            shops: [1]
                        }, {
                            city: 20,
                            shops: [1, 49]
                        }, {
                            city: 21,
                            shops: [1, 31]
                        }, {
                            city: 22,
                            shops: [1]
                        }, {
                            city: 23,
                            shops: [1]
                        }, {
                            city: 24,
                            shops: [1, 49]
                        }, {
                            city: 25,
                            shops: [1, 48]
                        }, {
                            city: 26,
                            shops: [1, 48]
                        }, {
                            city: 27,
                            shops: [1]
                        }, {
                            city: 28,
                            shops: [1]
                        }, {
                            city: 29,
                            shops: [1]
                        }, {
                            city: 30,
                            shops: [1, 48, 49]
                        }, {
                            city: 31,
                            shops: [1, 49]
                        }, {
                            city: 32,
                            shops: [1, 48, 49]
                        }, {
                            city: 33,
                            shops: [1, 49]
                        }, {
                            city: 34,
                            shops: [1, 48, 49]
                        }, {
                            city: 35,
                            shops: [1, 36, 48]
                        }, {
                            city: 37,
                            shops: [1, 48, 49]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 40,
                            shops: [32]
                        }, {
                            city: 41,
                            shops: [33]
                        }, {
                            city: 42,
                            shops: [40, 42, 47]
                        }, {
                            city: 43,
                            shops: [42, 48, 49]
                        }, {
                            city: 44,
                            shops: [48]
                        }, {
                            city: 45,
                            shops: [48]
                        }, {
                            city: 46,
                            shops: [48, 49]
                        }, {
                            city: 47,
                            shops: [48]
                        }, {
                            city: 48,
                            shops: [48]
                        }, {
                            city: 49,
                            shops: [48, 49]
                        }, {
                            city: 50,
                            shops: [48]
                        }, {
                            city: 51,
                            shops: [48]
                        }, {
                            city: 52,
                            shops: [48]
                        }, {
                            city: 53,
                            shops: [48]
                        }, {
                            city: 54,
                            shops: [48]
                        }, {
                            city: 55,
                            shops: [48]
                        }, {
                            city: 56,
                            shops: [48]
                        }, {
                            city: 57,
                            shops: [48]
                        }, {
                            city: 58,
                            shops: [48]
                        }, {
                            city: 59,
                            shops: [48]
                        }, {
                            city: 60,
                            shops: [49]
                        }, {
                            city: 61,
                            shops: [49]
                        }, {
                            city: 62,
                            shops: [49]
                        }, {
                            city: 63,
                            shops: [49]
                        }, {
                            city: 64,
                            shops: [49]
                        }]
                    }
                },
                barista: {
                    "no-4": {
                        cities: [{
                            city: 1,
                            shops: [1, 20, 23, 24, 48]
                        }, {
                            city: 2,
                            shops: [1]
                        }, {
                            city: 3,
                            shops: [1]
                        }, {
                            city: 4,
                            shops: [1]
                        }, {
                            city: 5,
                            shops: [1, 48]
                        }, {
                            city: 6,
                            shops: [1]
                        }, {
                            city: 7,
                            shops: [1, 49]
                        }, {
                            city: 8,
                            shops: [1, 48, 49]
                        }, {
                            city: 9,
                            shops: [1, 48, 49]
                        }, {
                            city: 10,
                            shops: [1]
                        }, {
                            city: 11,
                            shops: [1, 48]
                        }, {
                            city: 12,
                            shops: [1, 49]
                        }, {
                            city: 13,
                            shops: [1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 48]
                        }, {
                            city: 14,
                            shops: [1, 48]
                        }, {
                            city: 15,
                            shops: [1, 48]
                        }, {
                            city: 16,
                            shops: [1, 49]
                        }, {
                            city: 17,
                            shops: [1]
                        }, {
                            city: 18,
                            shops: [1]
                        }, {
                            city: 19,
                            shops: [1, 48]
                        }, {
                            city: 20,
                            shops: [1, 49]
                        }, {
                            city: 21,
                            shops: [1, 46]
                        }, {
                            city: 22,
                            shops: [1]
                        }, {
                            city: 23,
                            shops: [1]
                        }, {
                            city: 24,
                            shops: [1, 49]
                        }, {
                            city: 25,
                            shops: [1, 48]
                        }, {
                            city: 26,
                            shops: [1, 48]
                        }, {
                            city: 27,
                            shops: [1]
                        }, {
                            city: 28,
                            shops: [1]
                        }, {
                            city: 29,
                            shops: [1]
                        }, {
                            city: 30,
                            shops: [1, 48, 49]
                        }, {
                            city: 31,
                            shops: [1, 49]
                        }, {
                            city: 32,
                            shops: [1, 48, 49]
                        }, {
                            city: 33,
                            shops: [1, 49]
                        }, {
                            city: 34,
                            shops: [1, 48, 49]
                        }, {
                            city: 35,
                            shops: [1, 36, 48, 50]
                        }, {
                            city: 36,
                            shops: [1]
                        }, {
                            city: 37,
                            shops: [1, 48, 49]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 43,
                            shops: [42, 48, 49]
                        }, {
                            city: 44,
                            shops: [48]
                        }, {
                            city: 45,
                            shops: [48]
                        }, {
                            city: 46,
                            shops: [48, 49]
                        }, {
                            city: 47,
                            shops: [48]
                        }, {
                            city: 48,
                            shops: [48]
                        }, {
                            city: 49,
                            shops: [48, 49]
                        }, {
                            city: 50,
                            shops: [48]
                        }, {
                            city: 51,
                            shops: [48]
                        }, {
                            city: 52,
                            shops: [48]
                        }, {
                            city: 53,
                            shops: [48]
                        }, {
                            city: 54,
                            shops: [48]
                        }, {
                            city: 55,
                            shops: [48]
                        }, {
                            city: 56,
                            shops: [48]
                        }, {
                            city: 57,
                            shops: [48]
                        }, {
                            city: 58,
                            shops: [48]
                        }, {
                            city: 59,
                            shops: [48]
                        }, {
                            city: 60,
                            shops: [49]
                        }, {
                            city: 61,
                            shops: [49]
                        }, {
                            city: 62,
                            shops: [49]
                        }, {
                            city: 63,
                            shops: [49]
                        }, {
                            city: 64,
                            shops: [49]
                        }]
                    },
                    "no-7": {
                        cities: [{
                            city: 1,
                            shops: [1, 16, 20, 21, 22, 23, 25, 26, 29, 48]
                        }, {
                            city: 2,
                            shops: [1]
                        }, {
                            city: 3,
                            shops: [1]
                        }, {
                            city: 4,
                            shops: [1, 35]
                        }, {
                            city: 5,
                            shops: [1, 48]
                        }, {
                            city: 6,
                            shops: [1]
                        }, {
                            city: 7,
                            shops: [1, 49]
                        }, {
                            city: 8,
                            shops: [1, 48, 49]
                        }, {
                            city: 9,
                            shops: [1, 48, 49]
                        }, {
                            city: 10,
                            shops: [1]
                        }, {
                            city: 11,
                            shops: [1, 48]
                        }, {
                            city: 12,
                            shops: [1, 49]
                        }, {
                            city: 13,
                            shops: [1, 2, 3, 5, 7, 8, 8, 10, 11, 12, 13, 18, 48]
                        }, {
                            city: 14,
                            shops: [1, 48]
                        }, {
                            city: 15,
                            shops: [1, 48]
                        }, {
                            city: 16,
                            shops: [1, 49]
                        }, {
                            city: 17,
                            shops: [1]
                        }, {
                            city: 18,
                            shops: [1]
                        }, {
                            city: 19,
                            shops: [1, 48]
                        }, {
                            city: 20,
                            shops: [1, 49]
                        }, {
                            city: 21,
                            shops: [1, 46]
                        }, {
                            city: 22,
                            shops: [1]
                        }, {
                            city: 23,
                            shops: [1]
                        }, {
                            city: 24,
                            shops: [1, 49]
                        }, {
                            city: 25,
                            shops: [1, 48]
                        }, {
                            city: 26,
                            shops: [1, 48]
                        }, {
                            city: 27,
                            shops: [1]
                        }, {
                            city: 28,
                            shops: [1]
                        }, {
                            city: 29,
                            shops: [1]
                        }, {
                            city: 30,
                            shops: [1, 48, 49]
                        }, {
                            city: 31,
                            shops: [1, 49]
                        }, {
                            city: 32,
                            shops: [1, 48, 49]
                        }, {
                            city: 33,
                            shops: [1, 49]
                        }, {
                            city: 34,
                            shops: [1, 48, 49]
                        }, {
                            city: 35,
                            shops: [1, 36, 48]
                        }, {
                            city: 36,
                            shops: [1]
                        }, {
                            city: 37,
                            shops: [1, 48, 49]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 40,
                            shops: [32]
                        }, {
                            city: 41,
                            shops: [33]
                        }, {
                            city: 42,
                            shops: [40]
                        }, {
                            city: 43,
                            shops: [42, 48, 49]
                        }, {
                            city: 44,
                            shops: [48]
                        }, {
                            city: 45,
                            shops: [48]
                        }, {
                            city: 46,
                            shops: [48, 49]
                        }, {
                            city: 47,
                            shops: [48]
                        }, {
                            city: 48,
                            shops: [48]
                        }, {
                            city: 49,
                            shops: [48, 49]
                        }, {
                            city: 50,
                            shops: [48]
                        }, {
                            city: 51,
                            shops: [48]
                        }, {
                            city: 52,
                            shops: [48]
                        }, {
                            city: 53,
                            shops: [48]
                        }, {
                            city: 54,
                            shops: [48]
                        }, {
                            city: 55,
                            shops: [48]
                        }, {
                            city: 56,
                            shops: [48]
                        }, {
                            city: 57,
                            shops: [48]
                        }, {
                            city: 58,
                            shops: [48]
                        }, {
                            city: 59,
                            shops: [48]
                        }, {
                            city: 60,
                            shops: [49]
                        }, {
                            city: 61,
                            shops: [49]
                        }, {
                            city: 62,
                            shops: [49]
                        }, {
                            city: 63,
                            shops: [49]
                        }, {
                            city: 64,
                            shops: [49]
                        }]
                    },
                    "no-11": {
                        cities: [{
                            city: 1,
                            shops: [16, 20, 21, 22, 23]
                        }, {
                            city: 7,
                            shops: [49]
                        }, {
                            city: 8,
                            shops: [49]
                        }, {
                            city: 9,
                            shops: [49]
                        }, {
                            city: 12,
                            shops: [49]
                        }, {
                            city: 13,
                            shops: [11, 12, 8, 9]
                        }, {
                            city: 16,
                            shops: [49]
                        }, {
                            city: 20,
                            shops: [49]
                        }, {
                            city: 24,
                            shops: [49]
                        }, {
                            city: 25,
                            shops: [4]
                        }, {
                            city: 30,
                            shops: [49]
                        }, {
                            city: 31,
                            shops: [49]
                        }, {
                            city: 32,
                            shops: [49]
                        }, {
                            city: 33,
                            shops: [49]
                        }, {
                            city: 34,
                            shops: [49]
                        }, {
                            city: 35,
                            shops: [36]
                        }, {
                            city: 37,
                            shops: [49]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 40,
                            shops: [32]
                        }, {
                            city: 43,
                            shops: [42, 49]
                        }, {
                            city: 46,
                            shops: [49]
                        }, {
                            city: 49,
                            shops: [49]
                        }, {
                            city: 60,
                            shops: [49]
                        }, {
                            city: 61,
                            shops: [49]
                        }, {
                            city: 62,
                            shops: [49]
                        }, {
                            city: 63,
                            shops: [49]
                        }, {
                            city: 64,
                            shops: [49]
                        }]
                    }
                },
                "mon-ami": {
                    bright: {
                        cities: [{
                            city: 1,
                            shops: [27]
                        }, {
                            city: 13,
                            shops: [10, 11, 14, 18]
                        }, {
                            city: 35,
                            shops: [36, 50]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 41,
                            shops: [33]
                        }, {
                            city: 43,
                            shops: [42]
                        }]
                    },
                    delight: {
                        cities: [{
                            city: 1,
                            shops: [24, 27]
                        }, {
                            city: 13,
                            shops: [10, 11, 14, 9]
                        }, {
                            city: 21,
                            shops: [46]
                        }, {
                            city: 35,
                            shops: [36]
                        }, {
                            city: 39,
                            shops: [30]
                        }, {
                            city: 42,
                            shops: [40]
                        }, {
                            city: 43,
                            shops: [42]
                        }]
                    }
                }
            }
        };
        return a.prototype.init = function() {
            this.initialized = !0;
            var a, b, c = document.getElementById("map");
            for (a in this.products) {
                this.maps[a] = [];
                for (b in this.products[a]) {
                    var d = new Map(this.products[a][b], c);
                    this.maps[a][b] = d
                }
            }
            if ("undefined" != typeof pageModel) {
                var e = pageModel.getHashes();
                this.update(e[2], e[3])
            }
        }, a.prototype.unload = function() {
            this.initialized = !1
        }, a.prototype.update = function(a, b) {
            if (this.initialized) {
                $("#map-control").find("a").removeClass("active");
                var c = $('#map-control a[href*="' + a + "/" + b + '"]');
                c.addClass("active"), $(".map-images").removeClass("active"), $(".map-images." + a).addClass("active");
                var d = c.data("title");
                titleModel.set(d), $("#container").find("h2").html(d);
                var e = this.maps[a][b];
                e != this.currentMap && (this.currentMap && this.currentMap.animateOut(), e && e.animateIn(), this.currentMap = e)
            }
        }, a
    }(),
    PageModel = function() {
        "use strict";
        var a = function() {
            this.cacheY = 0, this.isOpen = !1, this.scrollTop = 0, this.currentModel = "", this.loadedHash = null, this.currentColor = null, this.isMoving = !1, this.hashes
        };
        return a.prototype.update = function(a) {
            this.scrollTop = a
        }, a.prototype.navigate = function() {
            var a = History.getState(),
                b = this,
                c = a.hash || "",
                d = c.substr(1).split("/");
            window.language = d[0], d.length < 3 && (this.isOpen === !1 ? this.move(d[1]) : (this.close(), this.isOpen = !1)), d.length > 2 && ($("#close").attr("href", "/" + d[0] + "/" + d[1]), this.loadedHash !== c && this.loadPage(c)), b.hashes = d, b.loadedHash = c
        }, a.prototype.getHashes = function() {
            return this.hashes
        }, a.prototype.loadPage = function(a) {
            var b = this;
            b.isOpen === !1 && this.loading(a), "/" != a.substring(0, 1) && (a = "/" + a), $("#extended").load(a + " #container", function() {
                var a = $(this).find("#container"),
                    c = a.attr("data-color") || "gray",
                    d = a.attr("data-script"),
                    e = a.attr("data-title") || "";
                return titleModel.set(e), b.currentColor = colorModel.getColorByName(c).light, colorModel.setPageColor(c), b.currentModel, b.currentModel = d, b.isOpen === !1 ? void b.open(a) : void b.loadAssets(a)
            })
        }, a.prototype.loadAssets = function(a) {
            var b = this,
                c = a.find(".spider-chart");
            if (c.length > 0) {
                for (var d = 0; d < c.length; d++) {
                    var e = c[d];
                    new SpiderGraph(e, b.currentColor, e.getAttribute("data-spider"))
                }
                $(window).trigger("resize")
            }
            $("#coffee-label").length > 0 && this.animateCoffeeLabel()
        }, a.prototype.loading = function(a) {
            this.cacheY = this.scrollTop, this.loadedHash !== a && $("#extended").empty(), TweenMax.set($(".extend-page"), {
                top: this.cacheY
            }), TweenMax.to($(".main-page"), .5, {
                left: -50
            }), TweenMax.to($(".extend-page"), .5, {
                left: window.innerWidth - 100,
                display: "block"
            })
        }, a.prototype.open = function(a) {
            var b = this;
            TweenMax.to($("#main"), .2, {
                autoAlpha: 0
            }), TweenMax.to($("#close"), .2, {
                autoAlpha: 1
            }), TweenMax.to($(".main-page"), .8, {
                left: -window.innerWidth,
                ease: Power0.easeIn,
                onComplete: function() {
                    return $(".main-page").css({
                        display: "none"
                    }), b.currentModel ? void window[b.currentModel].init() : void 0
                }
            }), TweenMax.to($(".extend-page"), .8, {
                left: 0,
                display: "block",
                onComplete: function() {
                    $("#wrapper").scrollTop(0), TweenMax.set($(".extend-page"), {
                        top: 0
                    }), b.loadAssets(a)
                }
            }), this.isOpen = !0
        }, a.prototype.close = function() {
            var a = this;
            TweenMax.to($("#main"), .2, {
                autoAlpha: 1
            }), TweenMax.to($("#close"), .2, {
                autoAlpha: 0
            }), TweenMax.to($(".main-page"), .8, {
                left: 0,
                display: "block"
            }), TweenMax.to($(".extend-page"), .8, {
                left: window.innerWidth,
                ease: Power0.easeIn,
                onStart: function() {
                    $(".main-page").css({
                        display: "block"
                    }), TweenMax.set($(".extend-page"), {
                        top: a.cacheY
                    }), $("#wrapper").scrollTop(a.cacheY)
                },
                onComplete: function() {
                    TweenMax.set($(".extend-page"), {
                        display: "none"
                    })
                }
            }), window.pageChange = !1, this.isOpen = !1, $(window).trigger("scroll")
        }, a.prototype.move = function(a) {
            var b = $("#" + a),
                c = 0,
                d = this;
            d.isMoving = !0, b.length > 0 && (c = b.position().top), TweenMax.to($("#wrapper"), 1, {
                scrollTo: c,
                onComplete: function() {
                    d.isMoving = !1
                }
            })
        }, a.prototype.animateCoffeeLabel = function() {
            TweenMax.to($("#coffee-label"), 1, {
                autoAlpha: .3,
                repeat: -1,
                yoyo: !0,
                ease: "Sine.easeInOut"
            })
        }, a
    }(),
    NavigationModel = function() {
        "use strict";
        var a = function() {
            var a = this;
            a.percentage = 0, a.topPosition = 0, a.mouseOver = !1, $("#wrapper").on({
                click: function(a) {
                    if (window.localChange = !1, $(this).hasClass("readmore") || $(this).hasClass("readless")) {
                        a.preventDefault();
                        var b = $(this),
                            c = b.data().id,
                            d = "#" + b.data().target;
                        b.hasClass("readmore") && (b.parent().parent().find(d).slideDown("fast"), b.fadeOut("fast"), $("#article-contract-" + c).fadeIn("fast")), b.hasClass("readless") && (b.parent().parent().find(d).slideUp("fast"), b.fadeOut("fast"), $("#article-expand-" + c).fadeIn("fast"))
                    } else {
                        var e = $(this).attr("href");
                        if ($(this).is("#close") && (window.pageChange = !1), -1 === e.indexOf("https://") && -1 === e.indexOf("http://") && -1 === e.indexOf("blog")) {
                            a.preventDefault();
                            var f = History.getState();
                            f.hash === e ? router.perform() : router.navigate($(this).attr("href"), null, null)
                        }
                    }
                }
            }, "a"), $("#main").on({
                mouseenter: function() {
                    TweenMax.to($(".pointer"), .3, {
                        top: $(this).position().top + 2
                    }), a.mouseOver = !0
                }
            }, "a"), $("#main").on({
                mouseleave: function() {
                    TweenMax.to($(".pointer"), .3, {
                        top: a.topPosition
                    }), a.mouseOver = !1
                }
            })
        };
        return a.prototype.setActive = function(a) {
            a.parents("ul").find("a").removeClass("active"), a.addClass("active")
        }, a.prototype.update = function(a) {
            if (!pageModel.isOpen && !pageModel.isMoving) {
                var b = this,
                    c = $("#main").find("[href*=" + a.name + "]");
                if (c.length > 0) {
                    var d = c.position().top - 5 || 0,
                        e = c.height(),
                        f = a.percentage;
                    b.topPosition = d + e * (f / 100), b.mouseOver || TweenMax.to($(".pointer"), .1, {
                        top: b.topPosition
                    }), window.localChange = !0, router.navigate(c.attr("href"), null, null)
                }
                return !1
            }
        }, a
    }(),
    Positioner = function() {
        "use strict";
        var a = function() {};
        return a.prototype.update = function(a) {
            window.innerWidth > 860 && (this.background(a), this.objects(a), this.horizontalObjects(a), this.navigation(a))
        }, a.prototype.horizontalObjects = function(a) {
            for (var b = 0, c = a.horizontalObjects, d = a.horizontalObjects.length; d > b; b++) c[b].style.backgroundPosition = 10 * a.percentage + "px 0";
            return a
        }, a.prototype.background = function(a) {
            return a.background ? (TweenMax.set(a.el, {
                backgroundPosition: "50% " + a.percentage + "%"
            }), a) : void 0
        }, a.prototype.objects = function(a) {
            for (var b = -10 * a.percentage, c = 0, d = a.objects, e = a.objects.length; e > c; c++) {
                var f = d[c],
                    g = parseFloat(f.getAttribute("data-posy")) || 1,
                    h = parseFloat(f.getAttribute("data-speed")) || 1,
                    i = b * h + g;
                a.animation ? a.tm.updateTo({
                    top: Math.round(i)
                }, !0) : a.tm = TweenMax.to(f, .3, {
                    top: Math.round(i)
                })
            }
            return a
        }, a.prototype.navigation = function() {}, a
    }(),
    View = function() {
        "use strict";
        var a = function(a, b, c) {
            return this.el = a, this.inview = b, this.name = a.id, this.title = a.getAttribute("data-title") || !1, this.percentage = c, this.objects = a.querySelectorAll(".object"), this.horizontalObjects = a.querySelectorAll(".horizontal-bg"), this.background = $(a).hasClass("spread") || !1, this.height = a.offsetHeight, this.top = a.offsetTop, this.color = a.getAttribute("data-color"), this
        };
        return a.prototype.update = function(a, b) {
            return this.inview = a, this.percentage = b, this
        }, a.prototype.resize = function() {
            this.height = this.el.offsetHeight, this.top = this.el.offsetTop
        }, a
    }(),
    ViewModel = function(a) {
        "use strict";
        var b = function(b) {
            this.views = [], this.vph = getViewportHeight();
            for (var c = 0, d = b, e = b.length; e > c; c++) {
                var f = d[c],
                    g = 0,
                    h = !1;
                this.views.push(new View(f, h, g))
            }
            this.inview(0);
            var i = this;
            $(a).on({
                resize: function() {
                    i.vph = getViewportHeight(), i.resize()
                }
            })
        };
        return b.prototype.inview = function(a) {
            var b = [],
                c = this.vph;
            a = a || 0;
            for (var d = 0, e = this.views, f = this.views.length; f > d; d++) {
                var g = e[d],
                    h = g.top,
                    i = g.height,
                    j = g.inview || !1,
                    k = Math.round(100 * (a - h) / g.height * 100) / 100;
                a >= h + i || h >= a + c ? j && g.update(!1, k) : (g.update(!0, k), b.push(g))
            }
            return b
        }, b.prototype.resize = function() {
            if (!pageModel.isOpen)
                for (var a = 0, b = this.views, c = this.views.length; c > a; a++) {
                    var d = b[a];
                    d.resize()
                }
        }, b
    }(window),
    ShareHelper = function() {
        "use strict";
        var a = function() {};
        return a.prototype.init = function() {
            addthis.toolbox(".addthis_toolbox"), addthis.counter(".addthis_counter"), addthis.init()
        }, a
    }();
$(function() {
    "use strict";

    function a() {
        window.localChange || window.pageChange || (i.navigate(), ("/en/heritage/specialty-coffee" == i.loadedHash || "/ru/heritage/specialty-coffee" == i.loadedHash) && n.init()), window.localChange = !1, ga("send", "pageview", History.getState().hash), ga("Skybox.send", "pageview", History.getState().hash)
    }

    function b() {
        if (!i.isOpen) {
            q = j.inview(r), i.update(r), k.navigation(q, r);
            for (var a = 0, b = q, c = q.length; c > a; a++) {
                var d = b[a];
                k.update(d), 0 === a && (o.update(d), d.title && t.set(d.title)), a == c - 1 && e !== d.color && (s.classList.add(d.color), s.classList.remove(e), e = d.color)
            }
        }
    }

    function c() {
        $(".wheretobuy-video-cover").hide(), $("#vimeoplayer").show()
    }

    function d() {
        $(window).width() < 860 && ($(".wheretobuy-video-cover").show(), $("#vimeoplayer").hide())
    }
    FastClick.attach(document.body); {
        var e, f = new staterouter.Router,
            g = $("#wrapper"),
            h = new ShareHelper,
            i = new PageModel,
            j = new ViewModel(document.querySelectorAll("section")),
            k = new Positioner,
            l = new MapModel,
            m = new ColorModel,
            n = new Icons,
            o = new NavigationModel,
            p = new CoffeeWheel,
            q = [],
            r = g.scrollTop(),
            s = document.getElementById("main"),
            t = new TitleModel;
        new Slider
    }
    window.colorModel = m, window.mapModel = l, window.pageModel = i, window.navigationModel = o, window.coffeeWheel = p, window.router = f, window.localChange = !1, window.pageChange = !1, window.titleModel = t, window.shareHelper = h, f.route("/:lang/:position", a), f.route("/:lang/:position/:page", a), f.route("/:lang/wheretobuy/:product/:type", function(b, c, d) {
        a(this), l.update(c, d);
        var e = "gray";
        switch (c) {
            case "mon-ami":
                e = "gray";
                break;
            case "paretto":
                e = "red";
                break;
            case "barista":
                e = "green"
        }
        m.setPageColor(e), window.pageChange = !0
    }), f.route("/:lang/:position/:page/:all", a), f.route("/:lang/:position/:page/:all/:all", a), f.perform(), TweenMax.set($(".extend-page"), {
        left: window.innerWidth,
        display: "none"
    });
    var u, v = 0,
        w = !1,
        x = 0;
    g.on({
            touchstart: function() {
                w = !0
            },
            touchend: function() {
                x = v, w = !1
            },
            touchmove: function(a) {
                x = v, v = u - a.originalEvent.pageY, u = a.originalEvent.pageY
            },
            mousewheel: function(a) {
                var b = a.originalEvent.wheelDeltaX || 0,
                    c = a.originalEvent.wheelDeltaY || 0;
                b || c || (c = a.originalEvent.wheelDelta || 0), 0 != c && a.preventDefault(), c > 0 ? (x = -1, v -= Math.round(.1 * c)) : (x = 1, v -= Math.round(.1 * c))
            },
            scroll: function(a) {
                a.preventDefault(), r = Math.abs($(this).scrollTop()), b()
            }
        }),
        function C() {
            if (requestAnimationFrame(C), !w) {
                if (0 > x) {
                    if (v > -1) return void(v = 0)
                } else if (1 > v) return void(v = 0);
                TweenMax.set(g, {
                    scrollTop: "+=" + v
                }), v *= .9
            }
        }();
    var y = window.innerWidth < 860 ? !0 : !1;
    window.onresize = function() {
        y && window.innerWidth > 860 && (g.trigger("scroll"), y = !1), window.innerWidth < 860 && (y = !0);
        var a = $(document.getElementById("container")).find(".spider-chart");
        if (0 != a.length) {
            if (window.innerWidth > 460) return a.width(420), void a.height(400);
            a.width(window.innerWidth - 55), a.height(.95 * (window.innerWidth - 55))
        }
    };
    var z = $(".navigation-wrap");
    $(document.getElementById("toggle-navigation")).on({
        click: function() {
            z.toggleClass("open"), z.hasClass("open") ? TweenMax.to(z, .5, {
                height: $("#main").outerHeight(!0) + $("#top").outerHeight(!0) + 40
            }) : TweenMax.to(z, .5, {
                height: 40
            })
        }
    }), z.on({
        click: function() {
            z.hasClass("open") && $(document.getElementById("toggle-navigation")).trigger("click")
        }
    }, "a"), $(".readmore, .readless").on({
        click: function(a) {
            a.preventDefault();
            var b = $(this),
                c = b.data().id,
                d = "#" + b.data().target;
            b.hasClass("readmore") && (b.parent().parent().find(d).slideDown("fast"), b.fadeOut("fast"), $("#article-contract-" + c).fadeIn("fast")), b.hasClass("readless") && (b.parent().parent().find(d).slideUp("fast"), b.fadeOut("fast"), $("#article-expand-" + c).fadeIn("fast"))
        }
    }), $("#social-link").on({
        click: function(a) {
            a.preventDefault(), TweenMax.to($("#social-block"), .5, {
                top: 0
            })
        }
    }), $(".close-social").on({
        click: function(a) {
            a.preventDefault(), TweenMax.to($("#social-block"), .5, {
                top: "-100%"
            })
        }
    });
    var A, B = $("#vimeoplayer");
    B.length > 0 && (A = $f(B[0]), A.addEvent("ready", function() {
        A.addEvent("pause", d), A.addEvent("finish", d)
    })), $(".wheretobuy-video-container, .video-play, .video-frame").on({
        click: function(a) {
            a.preventDefault(), c(), A.api("play")
        }
    })
});